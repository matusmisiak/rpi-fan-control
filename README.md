# RPi Fan Control

Simple script for PWM fan control

## Usage

You can simply change the parameters at the beginning of the script.

To start the script on boot just add the following line to `/etc/rc.local` (before `exit 0`):

    python3 <path-to-this-repo>/rpi_fan_control.py &

Change `<path-to-this-repo>` to your path.

## Behavior

- If CPU temperature is greater than `TRES_LOW_OFF`, the fan on pin `FAN_PIN` starts cooling at speed directly proportional to the temperature:

> `TRES_LOW_ON` °C → `MIN_SPEED` %  
> `TRES_HIGH` °C → 100 %

- The fan stops cooling at temperature `TRES_LOW_ON`.
- If temperature is greater than `TRES_HIGH`, the fan spins at 100 %.
- The script checks temperature every `SLEEP` seconds.

> Note: You can safely kill this process using SIGTERM or SIGINT
