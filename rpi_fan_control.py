import RPi.GPIO as gpio
from gpiozero import CPUTemperature
import signal
import time
import sys

FAN_PIN = 14
TRES_LOW_ON = 45
TRES_LOW_OFF = 50
TRES_HIGH = 65
MIN_SPEED = 40
SLEEP = 5

def change_speed(speed):
    fan.stop()
    if speed > 0:
        fan.start(speed)

def end_program(signum, frame):
    print('Terminating...')
    fan.stop()
    gpio.cleanup()
    sys.exit(0)

gpio.setmode(gpio.BCM)
gpio.setup(FAN_PIN, gpio.OUT)

signal.signal(signal.SIGTERM, end_program)
signal.signal(signal.SIGINT, end_program)

fan = gpio.PWM(FAN_PIN, 25000)
speed = 0

while True:
    temp = CPUTemperature().temperature
    fan_on = speed != 0
    speed = (temp-TRES_LOW_ON)/(TRES_HIGH-TRES_LOW_ON)*(100-MIN_SPEED)+MIN_SPEED
    if (fan_on and temp < TRES_LOW_ON) or (not fan_on and temp < TRES_LOW_OFF):
        speed = 0
    if temp >= TRES_HIGH:
        speed = 100
    print(f'{temp:.2f}°C - {speed:.2f}%')
    change_speed(speed)
    time.sleep(SLEEP)
